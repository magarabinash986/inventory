﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace Inventory.Services
{
    public class ProductService
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-737OAU2\\ABINASH;Initial Catalog=Inventory;Integrated Security=True");

        public string Get()
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Products", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(dt);
            }
            else
            {
                return "No data found";
            }
        }

        public string Create(string value)
        {
            SqlCommand cmd = new SqlCommand("INSERT INTO Products VALUES(2,'"+ value +"', 3000, 2)", con);
            con.Open();
            int res = cmd.ExecuteNonQuery();
            con.Close();
            if (res == 1)
            {
                return "New Product Added";
            }
            else
            {
                return "Failed to Add new Product";
            }
        }
    }
}

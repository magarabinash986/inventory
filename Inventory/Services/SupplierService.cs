﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace Inventory.Services
{
    public class SupplierService
    {
        SqlConnection con = new SqlConnection("Data Source=DESKTOP-737OAU2\\ABINASH;Initial Catalog=Inventory;Integrated Security=True");

        public string Get()
        {
            SqlDataAdapter da = new SqlDataAdapter("SELECT * FROM Supplier", con);
            DataTable dt = new DataTable();
            da.Fill(dt);
            if (dt.Rows.Count > 0)
            {
                return JsonConvert.SerializeObject(dt);
            }
            else
            {
                return "No data found";
            }
        }
    }
}

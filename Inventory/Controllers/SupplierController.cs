﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Inventory.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Inventory.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SupplierController : ControllerBase
    {
        private readonly SupplierService _suupplierService;

        public SupplierController(SupplierService supplierService)
        {
            _suupplierService = supplierService;
        }

        [HttpGet]
        public string Get()
        {
            return _suupplierService.Get();
        }
    }
}

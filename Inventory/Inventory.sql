CREATE DATABASE Inventory;

CREATE TABLE Supplier (
id int primary key auto increment,
Sname varchar(50) not null,
Saddress varchar(80) not null,
Semail varchar(50),
Scontact varchar(10)
);

drop table Supplier;

CREATE TABLE Product (
id int primary key auto increment,
Pname varchar(30) not null,
Pprice int not null,
Psupplier int not null,
CONSTRIANT Psupplier FOREIGN KEY (id) references Inventory.Supplier(id)
);
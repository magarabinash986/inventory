﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class ProductSupplier
    {
        public int Product { get; set; }
        public int Supplier { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Inventory.Models
{
    public class ProductSupplierContext : DbContext
    {
        public ProductSupplierContext(DbContextOptions<ProductSupplierContext> options)
            : base(options)
        {
        }
        public DbSet<ProductSupplierContext> ProductSuppliers { get; set; }
    }
}

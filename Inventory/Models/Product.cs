﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class Product
    {
        public int Pid { get; set; }
        public string Pname { get; set; }
        public int Pprice { get; set; }
        public int Pqty { get; set; }
    }
}

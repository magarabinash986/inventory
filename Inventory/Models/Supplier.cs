﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Inventory.Models
{
    public class Supplier
    {
        public int Sid { get; set; }
        public string Sname { get; set; }
        public string Saddress { get; set; }
        public string Semail { get; set; }
        public string Contact { get; set; }
    }
}
